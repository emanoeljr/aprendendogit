import React from 'react'
import ReactDOM from 'react-dom'

// import Primeiro from './componentes/Primeiro'
// import BomDia from './componentes/BomDia'
// import Multi from './componentes/Multiplos'
// import Saudacao from './componentes/Saudacao'
import Pai from './componentes/Pai'
import Filho from './componentes/Filho'

ReactDOM.render(
    <div>
        <Pai nome="Emanoel" sobrenome="Chaves">
            {/* Passando os componentes Filhos */}
            <Filho nome="Pedro" />
            <Filho nome="Manuela" />
            <Filho nome="Maria Clara" />
        </Pai>
    </div>
,document.getElementById('root'))
